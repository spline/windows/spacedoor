create table events (
    event_id Integer primary key autoincrement not null,
    space_id Text not null,
    timestamp BigInt not null,
    door_open Boolean not null
);
