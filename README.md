# SpaceAPI Server

This is a SpaceAPI server with door state update functionality.

## Development setup

Install diesel cli:
```
cargo install diesel_cli --features sqlite --no-default-features
```

Run database migrations
```
diesel migration run
```

compile and start the application:
```
cargo run
```
