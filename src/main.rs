#[macro_use]
extern crate rocket;
extern crate diesel;

use rocket::{
    fs::FileServer, http::Status, request, request::FromRequest, request::Outcome,
    request::Request, response::status::Custom,
};
use rocket_sync_db_pools::database;

use diesel::{prelude::*, Insertable};

use std::{error::Error, time::SystemTime};

use serde::Deserialize;

mod schema;
mod spaceapi;
mod ui;
mod update;

use schema::*;

#[database("spaceapi")]
pub struct Db(diesel::SqliteConnection);

pub type Result<T> = std::result::Result<T, Custom<()>>;

pub struct AuthenticationHeader {
    token: String,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthenticationHeader {
    type Error = ();

    async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        let header = req.headers().get("spaceapi-token").next();
        match header {
            Some(token) => Outcome::Success(AuthenticationHeader {
                token: token.to_string(),
            }),
            None => Outcome::Error((Status::Unauthorized, ())),
        }
    }
}

pub fn handle_io_error<T: Error>(error: T) -> Custom<()> {
    eprintln!("io error occured: {:?}", error);
    Custom(Status::InternalServerError, ())
}

#[derive(Deserialize)]
pub struct SpaceConfig {
    space_id: String,
    update_token: String,
    defaults: spaceapi::SpaceApi,
}

#[launch]
fn rocket() -> _ {
    let rocket = rocket::build()
        .attach(Db::fairing())
        .mount(
            "/",
            routes![
                update::open_door,
                update::close_door,
                spaceapi::space_api,
                ui::status
            ],
        )
        .mount("/static/", FileServer::from("static"));

    let spaceapi_config = rocket
        .figment()
        .extract_inner::<Vec<SpaceConfig>>("spaces")
        .expect("Loading config");

    rocket.manage(spaceapi_config)
}
