use crate::*;

pub fn now_in_unix() -> i64 {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs()
        .try_into()
        .expect("Value doesn't fit into i64 anymore. Time to upgrade!")
}

#[derive(Insertable)]
#[diesel(table_name = events)]
struct InsertableEvent {
    timestamp: i64,
    space_id: String,
    door_open: bool,
}

fn check_can_update_space(
    auth: AuthenticationHeader,
    spaces: &rocket::State<Vec<SpaceConfig>>,
    space: &str,
) -> Result<()> {
    let space_config = spaces
        .iter()
        .find(|s| s.space_id == space)
        .ok_or(Custom(Status::NotFound, ()))?;

    if space_config.update_token != auth.token {
        return Err(Custom(Status::Unauthorized, ()));
    }

    Ok(())
}

async fn update_door_state(db: Db, space: String, door_open: bool) -> Result<()> {
    db.run(move |c| {
        diesel::insert_into(events::table)
            .values(InsertableEvent {
                timestamp: now_in_unix(),
                space_id: space,
                door_open,
            })
            .execute(c)
    })
    .await
    .map_err(handle_io_error)?;
    Ok(())
}

#[post("/<space>/door/open")]
pub async fn open_door(
    spaces: &rocket::State<Vec<SpaceConfig>>,
    auth: AuthenticationHeader,
    db: Db,
    space: String,
) -> Result<()> {
    check_can_update_space(auth, spaces, &space)?;
    update_door_state(db, space, true).await
}

#[post("/<space>/door/close")]
pub async fn close_door(
    spaces: &rocket::State<Vec<SpaceConfig>>,
    auth: AuthenticationHeader,
    db: Db,
    space: String,
) -> Result<()> {
    check_can_update_space(auth, spaces, &space)?;
    update_door_state(db, space, false).await
}
