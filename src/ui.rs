use crate::{spaceapi::space_api, Db, Result, SpaceConfig};

use maud::{html, PreEscaped, DOCTYPE, Markup};

#[get("/<space>")]
pub async fn status(
    db: Db,
    spaces: &rocket::State<Vec<SpaceConfig>>,
    space: String,
) -> Result<Markup> {
    let status = space_api(db, spaces, space).await?;

    Ok(html! {
        (DOCTYPE)
        html lang="en" {
            head {
                meta name="viewport" content="width=device-width, initial-scale=1";
                link rel="stylesheet" href="/static/css/leaflet.css";
                link rel="stylesheet" href="/static/css/bootstrap.min.css";
                title { (status.space) };
            }

            body {
                div class="text-center" {
                    img alt=(format!("{} logo", status.space)) style="max-height: 150px; max-width: 200px; object-fit: scale-down" src=(status.logo);
                    h1 { (status.space) }
                    p class="fs-4" { 
                        "is "
                        b {
                            (if status.state.open {
                                "open"
                            } else {
                                "closed"
                            })
                        }
                    }

                    div id="map" class="mw-100" style="width: 700px; height: 500px; display: block; margin-left: auto; margin-right: auto;" {};

                    p {
                        (status.location.address)

                        " ("

                        a href=(format!("geo:{},{}", status.location.lat, status.location.lon)) {
                            "view in app"
                        }

                        ")"
                    }

                    h2 { "Contact" }
                    ul style="list-style-type: none; margin: 0; padding: 0;" {
                        li {
                            a href=(status.url) { "website" }
                        }
                        @if let Some(ref contact) = status.contact {
                            @if let Some(ref email) = contact.email {
                                li {
                                    a href=(format!("mailto:{email}")) { "e-mail" }
                                }
                            }
                            @if let Some(ref ml) = contact.ml {
                                li {
                                    a href=(format!("mailto:{ml}")) { "mailing list" }
                                }
                            }
                        }
                    }

                    script src="/static/js/leaflet.js" {};

                    script {
                        (PreEscaped(format!(r#"
                        let loc = [{}, {}];

                        // Create map options
                        var mapOptions = {{
                            center: loc,
                            zoom: 50
                        }}

                        // Create a map object
                        var map = new L.map('map', mapOptions);

                        // Create a Layer object
                        var layer = new L.TileLayer('https://{{s}}.tile.openstreetmap.org/{{z}}/{{x}}/{{y}}.png', {{
                            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                        }});

                        // Add layer to the map
                        map.addLayer(layer);

                        // Add postion marker
                        var marker = L.marker(loc)
                        map.addLayer(marker)
                        "#, status.location.lat, status.location.lon)))
                    }
                }
            }
        }
    })
}
