use rocket::serde::json::Json;
use rocket::{http::Status, response::status::Custom};
use serde::{Deserialize, Serialize};

use diesel::prelude::*;

use crate::{handle_io_error, Db, Result, SpaceConfig};

use crate::schema::*;

#[derive(Serialize, Clone, Deserialize)]
pub struct Location {
    pub address: String,
    pub lon: f32,
    pub lat: f32,
}

#[derive(Serialize, Clone, Deserialize)]
pub struct Contact {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub irc: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ml: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub twitter: Option<String>,
}

#[derive(Serialize, Clone, Deserialize)]
pub struct Icon {
    pub open: String,
    pub closed: String,
}

#[derive(Serialize, Clone, Deserialize)]
pub struct State {
    pub open: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<Icon>,
}

fn spaceapi_version() -> String {
    "0.13".to_string()
}

fn spaceapi_compatible_versions() -> Vec<String> {
    vec!["14".to_string()]
}

#[derive(Serialize, Deserialize, Clone)]
pub struct SpaceApi {
    #[serde(default = "spaceapi_version")]
    pub api: String,
    #[serde(default = "spaceapi_compatible_versions")]
    pub api_compatibility: Vec<String>,
    pub space: String,
    pub logo: String,
    pub url: String,
    pub location: Location,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub contact: Option<Contact>,
    pub issue_report_channels: Vec<String>,
    pub state: State,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub projects: Option<Vec<String>>,
}

#[derive(Queryable)]
#[diesel(table_name = events)]
struct Event {
    _event_id: i32,
    _space_id: String,
    _timestamp: i64,
    door_open: bool,
}

#[get("/<space>/status")]
pub async fn space_api(
    db: Db,
    spaces: &rocket::State<Vec<SpaceConfig>>,
    space: String,
) -> Result<Json<SpaceApi>> {
    let space_id = space.clone();
    let last_event = db
        .run(|c| {
            events::table
                .filter(events::space_id.eq(space_id))
                .order_by(events::timestamp.desc())
                .limit(1)
                .get_result::<Event>(c)
                .optional()
        })
        .await
        .map_err(handle_io_error)?;

    let space = spaces
        .iter()
        .find(|s| s.space_id == space)
        .ok_or(Custom(Status::NotFound, ()))?;

    let mut response = space.defaults.clone();
    if let Some(event) = last_event {
        response.state.open = event.door_open;
    }
    Ok(Json(response))
}
