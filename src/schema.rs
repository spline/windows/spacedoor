// @generated automatically by Diesel CLI.

diesel::table! {
    events (event_id) {
        event_id -> Integer,
        space_id -> Text,
        timestamp -> BigInt,
        door_open -> Bool,
    }
}
